﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BSA21_Lecture1.WebLayer;

namespace BSA21_Lecture1.DesktopApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            dataGridView1.DataSource = bindingSource1;
            dataGridView2.DataSource = bindingSource2;
            dataGridView3.DataSource = bindingSource3;
            dataGridView4.DataSource = bindingSource4;
        }

        protected override async void OnLoad(EventArgs e)
        {
            bindingSource1.DataSource = await Requests.GetProjects();
            bindingSource2.DataSource = await Requests.GetTasks();
            bindingSource3.DataSource = await Requests.GetTeams();
            bindingSource4.DataSource = await Requests.GetUsers();
        }
    }
}
