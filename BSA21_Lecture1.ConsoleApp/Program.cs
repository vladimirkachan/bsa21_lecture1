﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BSA21_Lecture1.DataLayer;
using BSA21_Lecture1.WebLayer;
using BSA21_Lecture1.WebLayer.Schemas;

namespace BSA21_Lecture1.ConsoleApp
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Client client = new();
            //await FirstDemo(client);
            await DemoProjects(client);
            //await DemoPerformerTasks(client);
            //await DemoPerformerFinishedTaskInfo(client);
            //await DemoPerformerFinishedTask(client);
            //await DemoTeamInfo(client);
            //await DemoTeamsUsers(client);
            //await DemoUsersTasks(client);
            //await DemoPerformerProjectTaskInfo(client);
            //await DemoProjectWithMinMaxTask(client);
            Console.ReadKey();
        }
        static async Task DemoProjectWithMinMaxTask(Client client)
        {
            var (p, t1, t2, count) = await client.GetProjectWithMinMaxTask(5);
            Console.WriteLine("\tProject:");
            Console.WriteLine(
            $"{p.Id}. {p.Name} Author: {p.Author.LastName}, Team:{p.Team.Name}, {p.Deadline}\n\t{p.Description}");
            Console.WriteLine("\nTask with longest description:");
            Console.WriteLine(t1 == null ? "No task"
                              : $"{t1.Id}. {t1.Name} Project: '{t1.Project.Name}' - Performer: {t1.Performer.LastName}\n{t1.Description}\n{t1.CreatedAt} - {t1.FinishedAt}\n");
            Console.WriteLine("\nTask with the shortest name:");
            Console.WriteLine(t2 == null ? "No task"
                              : $"{t2.Id}. {t2.Name} Project: '{t2.Project.Name}' - Performer: {t2.Performer.LastName}\n{t2.Description}\n{t2.CreatedAt} - {t2.FinishedAt}\n");
            Console.WriteLine($"\nCount of users in the project team = {count}");
        }
        static async Task DemoPerformerProjectTaskInfo(Client client)
        {
            var (a, p, c1, c2, t) = await client.GetPerformerProjectTaskInfo(30);
            Console.WriteLine($"Performer: {a.Id}. {a.FirstName} {a.LastName} {a.BirthDay} {a.Team.Name}\n");
            Console.WriteLine("\tProject:");
            Console.WriteLine(
            $"{p.Id}. {p.Name} Author: {p.Author.LastName}, Team:{p.Team.Name}, {p.Deadline}\n\t{p.Description}");
            Console.WriteLine($"Total count of tasks = {c1}");
            Console.WriteLine($"Total count of unfinished or canceled tasks = {c2}");
            Console.WriteLine(t == null ? "No task"
                              : $"{t.Id}. {t.Name} Project: '{t.Project.Name}' - Performer: {t.Performer.LastName}\n{t.Description}\n{t.CreatedAt} - {t.FinishedAt}\n");
        }
        static async Task DemoUsersTasks(Client client)
        {
            var usersTasks = await client.GetUsersTasks(1, 3);
            foreach (var (user, tasks) in usersTasks)
            {
                Console.WriteLine($"\t{user.Id}. {user.FirstName} {user.LastName}, count of tasks = {tasks.Count()}");
                foreach (var t in tasks)
                    Console.WriteLine($"{t.Id}. {t.Name}\t Project: {t.Project.Name}");
                Console.WriteLine("\n----------------\n");
            }
        }
        static async Task DemoTeamsUsers(Client client)
        {
            var teamsUsers = await client.GetTeamsUsers();
            foreach (var (team, users) in teamsUsers)
            {
                Console.WriteLine(
                $"\t{team.Id}. {team.Name}: count = {users.Count()}, created at {team.CreatedAt.ToShortDateString()}");
                foreach (var u in users)
                    Console.WriteLine(
                    $"{u.Id}. {u.LastName} {u.FirstName}, {u.BirthDay}, registered at {u.RegisteredAt.ToShortDateString()}");
                Console.WriteLine("\t-------------------\n");
            }
        }
        static async Task DemoTeamInfo(Client client)
        {
            var teamInfos = await client.GetTeamInfo();
            foreach (var t in teamInfos)
            {
                Console.WriteLine($"\t{t.TeamId}. {t.TeamName}, count of users = {t.Count}, created at {t.CreatedAt.ToShortDateString()}");
                foreach (var u in t.Users)
                    Console.WriteLine($"{u.UserId}. {u.LastName} {u.FirstName} {u.Age} years old, , registered at {u.RegisteredAt.ToShortDateString()}");
                Console.WriteLine("----------------------\n");
            }
        }
        static async Task DemoPerformerFinishedTask(Client client)
        {
            var tasks = await client.GetFinishedTask(100, new DateTime(1999, 12, 31), DateTime.Now);
            foreach (var t in tasks)
                Console.WriteLine($"{t.Id}. '{t.Name}' Project: {t.Project.Name}, Performer: {t.Performer.LastName}");
        }
        static async Task DemoPerformerFinishedTaskInfo(Client client)
        {
            var info = await client.GetFinishedTaskInfo(100, new DateTime(2020, 12, 31), DateTime.Now);
            Console.WriteLine($"Count of selected tasks = {info.Count()}");
            foreach (var i in info)
                Console.WriteLine($"{i.TaskId}. {i.TaskName} - {i.FinishedAt} Performer: {i.PerformerName}");
        }
        static async Task DemoPerformerTasks(Client client)
        {
            var tasks = await client.GetTasksOnPerformer(33);
            Console.WriteLine($"\t Count = {tasks.Count()}");
            foreach (var t in tasks)
                Console.WriteLine(
                $"{t.Id}. {t.Name} {t.FinishedAt} Performer: {t.Performer.Id}. {t.Performer.LastName} {t.Performer.FirstName} ");
        }
        static async Task DemoProjects(Client client)
        {
            var projects = await client.Projects();
            Console.WriteLine($"count = {projects.Count()}");
            foreach (var p in projects)
            {
                Console.WriteLine($"\t{p.Id}. {p.Name} Author: {p.Author.LastName}, Team: {p.Team.Name}");
                var tasks = p.Tasks;
                Console.WriteLine($"count of task = {tasks.Count()}");
                foreach (var t in tasks)
                    Console.WriteLine($"t{t.Id} p{t.Project.Id} {t.Name} Performer: {t.Performer.LastName}");
            }
        }
        static async Task FirstDemo(Client client)
        {
            var count = await client.CountOfProjectTasks(3);
            Console.WriteLine($"Count of project tasks = {count}");
            count = await client.CountOfProjectTasksByPerformer(77, 67);
            Console.WriteLine($"Count of project tasks = {count}");
            var tasks = await client.GetProjectTasks(77);
            Console.WriteLine($"\ncount of tasks = {tasks.Count()}");
            foreach (var t in tasks)
                Console.WriteLine(
                $"{t.Id}. {t.Name}, project: {t.Project.Id} {t.Project.Name}, performer: {t.Performer.Id}. {t.Performer.LastName} {t.Performer.FirstName}");
            Console.WriteLine("\n------------\n");
            tasks = await client.GetProjectTaskByPerformer(77, 67);
            Console.WriteLine($"\ncount of tasks = {tasks.Count()}");
            foreach (var t in tasks)
                Console.WriteLine(
                $"{t.Id}. {t.Name}, project: {t.Project.Id} {t.Project.Name}, performer: {t.Performer.Id}. {t.Performer.LastName} {t.Performer.FirstName}");
            Console.WriteLine("\n------------\n");
            var countsByProjects = await client.CountsOfProjectTasks();
            Console.WriteLine("Project. Count");
            foreach (var (id, n) in countsByProjects)
                Console.WriteLine($"{id}. {n}");
            Console.WriteLine("\n------------\n");
        }
    }
}
