﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BSA21_Lecture1.DataLayer;
using BSA21_Lecture1.WebLayer;

namespace BSA21_Lecture1.ConsoleApp
{
    public class Client
    {
        //  - 1 -
        public async Task<int> CountOfProjectTasks(int projectId)
        {
            var taskSchemas = await Requests.GetTasks();
            return (from t in taskSchemas where t.ProjectId == projectId select t).Count();
        }
        public async Task<int> CountOfProjectTasksByPerformer(int projectId, int performerId)
        {
            var taskSchemas = await Requests.GetTasks();
            return (from t in taskSchemas
                    where t.ProjectId == projectId
                    where t.PerformerId == performerId
                    select t).Count();
        }
        public async Task<IEnumerable<TaskData>> GetProjectTasks(int projectId)
        {
            var taskSchemas = await Requests.GetTasks();
            var projectSchema = await Requests.GetProject(projectId);
            var userSchemas = await Requests.GetUsers();
            return from t in taskSchemas
                   where t.ProjectId == projectId
                   join u in userSchemas on t.PerformerId equals u.Id
                   select new TaskData
                   {
                       Id = t.Id,
                       Project = projectSchema.ToData(),
                       Performer = u.ToData(),
                       Name = t.Name,
                       Description = t.Description,
                       State = t.State,
                       CreatedAt = t.CreatedAt,
                       FinishedAt = t.FinishedAt
                   };
        }
        public async Task<IEnumerable<TaskData>> GetProjectTaskByPerformer(int projectId, int performerId)
        {
            var taskSchemas = await Requests.GetTasks();
            var projectSchema = await Requests.GetProject(projectId);
            var userSchemas = await Requests.GetUsers();
            return from t in taskSchemas
                   where t.ProjectId == projectId
                   where t.PerformerId == performerId
                   join u in userSchemas on t.PerformerId equals u.Id
                   select new TaskData
                   {
                       Id = t.Id,
                       Project = projectSchema.ToData(),
                       Performer = u.ToData(),
                       Name = t.Name,
                       Description = t.Description,
                       State = t.State,
                       CreatedAt = t.CreatedAt,
                       FinishedAt = t.FinishedAt
                   };

        }
        public async Task<Dictionary<int, int>> CountsOfProjectTasks()
        {
            var taskSchemas = await Requests.GetTasks();
            var query = from t in taskSchemas
                        group t by t.ProjectId into g
                        select KeyValuePair.Create(g.Key, g.Count());
            return new Dictionary<int, int>(query);
        }
        public async Task<IEnumerable<ProjectData>> Projects()
        {
            var projectSchemas = await Requests.GetProjects();
            var taskSchemas = await Requests.GetTasks();
            var teamSchemas = await Requests.GetTeams();
            var userSchemas = await Requests.GetUsers();

            var teamsQuery = from t in teamSchemas select new TeamData { Id = t.Id, Name = t.Name, CreatedAt = t.CreatedAt };
            var usersQuery = from u in userSchemas
                             select new UserData
                             {
                                 Id = u.Id,
                                 Team = teamsQuery.FirstOrDefault(team => team.Id == u.TeamId),
                                 FirstName = u.FirstName,
                                 LastName = u.LastName,
                                 Email = u.Email,
                                 RegisteredAt = u.RegisteredAt,
                                 BirthDay = u.BirthDay
                             };
            var projectsQuery = from p in projectSchemas
                                select new ProjectData
                                {
                                    Id = p.Id,
                                    Author = usersQuery.FirstOrDefault(user => user.Id == p.AuthorId),
                                    Team = teamsQuery.FirstOrDefault(team => team.Id == p.TeamId),
                                    Name = p.Name,
                                    Description = p.Description,
                                    Deadline = p.Deadline
                                };

            IEnumerable<TaskData> GetProjectTasks(int projectId, ProjectData projectData)
            {
                return from t in taskSchemas
                       where t.ProjectId == projectId
                       select new TaskData
                       {
                           Id = t.Id,
                           Project = projectData,
                           Performer = usersQuery.FirstOrDefault(user => user.Id == t.PerformerId),
                           Name = t.Name,
                           Description = t.Description,
                           State = t.State,
                           CreatedAt = t.CreatedAt,
                           FinishedAt = t.FinishedAt
                       };
            }

            var projects = projectsQuery.ToList();
            foreach (var p in projects)
                p.Tasks = GetProjectTasks(p.Id, p);
            return projects;
        }

        //  - 2 -
        public async Task<IEnumerable<TaskData>> GetTasksOnPerformer(int performerId, int maxLength = 45)
        {
            var userSchemas = await Requests.GetUsers();
            var taskSchemas = await Requests.GetTasks();
            return from t in taskSchemas
                   where t.PerformerId == performerId && t.Name.Length < maxLength
                   join u in userSchemas on t.PerformerId equals u.Id
                   select new TaskData
                   {
                       Id = t.Id,
                       Performer = u.ToData(),
                       Name = t.Name,
                       Description = t.Description,
                       State = t.State,
                       CreatedAt = t.CreatedAt,
                       FinishedAt = t.FinishedAt
                   };
        }

        //  - 3 -
        public async Task<IEnumerable<dynamic>> GetFinishedTaskInfo(int performerId, DateTime min, DateTime max)
        {
            var taskSchemas = await Requests.GetTasks();
            var userSchemas = await Requests.GetUsers();
            return from t in taskSchemas
                   where t.PerformerId == performerId && t.FinishedAt != null &&
                   t.FinishedAt > min && t.FinishedAt < max
                   join u in userSchemas on t.PerformerId equals u.Id
                   select new { TaskId = t.Id, TaskName = t.Name, t.FinishedAt, PerformerName = $"{u.FirstName} {u.LastName}" };
        }
        public async Task<IEnumerable<TaskData>> GetFinishedTask(int performerId, DateTime min, DateTime max)
        {
            var taskSchemas = await Requests.GetTasks();
            return from t in taskSchemas
                   where t.PerformerId == performerId && t.FinishedAt != null &&
                   t.FinishedAt > min && t.FinishedAt < max
                   let projectSchema = Requests.GetProject(t.ProjectId)
                   let userSchema = Requests.GetUser(t.PerformerId)
                   select new TaskData
                   {
                       Id = t.Id,
                       Project = projectSchema.Result.ToData(),
                       Performer = userSchema.Result.ToData(),
                       Name = t.Name,
                       Description = t.Description,
                       State = t.State,
                       CreatedAt = t.CreatedAt,
                       FinishedAt = t.FinishedAt
                   };
        }

        //  - 4 -
        public async Task<IEnumerable<dynamic>> GetTeamInfo(int minimumAge = 10)
        {
            var teamSchemas = await Requests.GetTeams();
            var userSchemas = await Requests.GetUsers();
            var query = from u in userSchemas
                        let age = DateTime.Now.Year - u.BirthDay.Year
                        where u.TeamId != null && age > minimumAge
                        join t in teamSchemas on u.TeamId equals t.Id
                        select new { UserId = u.Id, u.TeamId, u.FirstName, u.LastName, TeamName = t.Name, Age = age, t.CreatedAt, u.RegisteredAt };
            return from i in query
                   orderby i.CreatedAt descending, i.Age, i.RegisteredAt descending
                   group i by new { i.TeamId, i.TeamName, i.CreatedAt } into g
                   select new { g.Key.TeamId, g.Key.TeamName, Count = g.Count(), g.Key.CreatedAt, Users = g };
        }
        public async Task<IEnumerable<(TeamData, IEnumerable<UserData>)>> GetTeamsUsers(int minimumAge = 10)
        {
            var teamSchemas = await Requests.GetTeams();
            var userSchemas = await Requests.GetUsers();
            return from t in teamSchemas
                   orderby t.CreatedAt descending
                   let team = t.ToData()
                   select (
                   team,
                   from u in userSchemas
                   where u.TeamId == t.Id && DateTime.Now.Year - u.BirthDay.Year > minimumAge
                   orderby u.RegisteredAt descending
                   select new UserData
                   {
                       Id = u.Id,
                       Team = team,
                       FirstName = u.FirstName,
                       LastName = u.LastName,
                       Email = u.Email,
                       RegisteredAt = u.RegisteredAt,
                       BirthDay = u.BirthDay
                   });
        }

        //  - 5 -
        public async Task<IEnumerable<(UserData, IEnumerable<TaskData>)>> GetUsersTasks(int startUserId = 0, int endUserId = int.MaxValue)
        {
            var userSchemas = await Requests.GetUsers();
            var taskSchemas = await Requests.GetTasks();
            var projectSchemas = await Requests.GetProjects();
            return from u in userSchemas
                   where u.Id >= startUserId && u.Id <= endUserId
                   orderby u.FirstName, u.LastName
                   let user = u.ToData()
                   select (
                   user,
                   from t in taskSchemas
                   join p in projectSchemas on t.ProjectId equals p.Id
                   orderby t.Name.Length, t.Name.ToUpper()
                   select new TaskData
                   {
                       Id = t.Id,
                       Project = p.ToData(),
                       Performer = user,
                       Name = t.Name,
                       Description = t.Description,
                       State = t.State,
                       CreatedAt = t.CreatedAt,
                       FinishedAt = t.FinishedAt
                   });
        }

        //  - 6 - 
        public async Task<(UserData, ProjectData, int, int, TaskData)> GetPerformerProjectTaskInfo(int performerId)
        {
            var projectSchemas = await Requests.GetProjects();
            var userSchemas = await Requests.GetUsers();
            var taskSchemas = await Requests.GetTasks();
            var teamSchemas = await Requests.GetTeams();

            var performerSchema = userSchemas.FirstOrDefault(u => u.Id == performerId);

            var performerTaskSchemas = from t in taskSchemas where t.PerformerId == performerId
                                       orderby t.CreatedAt descending select t;
            var lastPerformerTaskSchema = performerTaskSchemas.FirstOrDefault();
            var lastProjectSchema = await Requests.GetProject(lastPerformerTaskSchema.ProjectId);

            var lastProjectTaskCount = (from t in taskSchemas where t.ProjectId == lastProjectSchema.Id
                                        select t).Count();

            var countNotFinishedTasks = (from t in performerTaskSchemas where t.FinishedAt == null || t.FinishedAt > DateTime.Now select t).Count();

            var longestTask = (from t in performerTaskSchemas
                               where t.FinishedAt != null
                               let span = t.FinishedAt - t.CreatedAt
                               orderby span descending
                               select t).FirstOrDefault();

            var performerData = performerSchema.ToData();
            performerData.Team = teamSchemas.FirstOrDefault(t => t.Id == performerSchema.TeamId).ToData();
            var projectData = lastProjectSchema.ToData();
            projectData.Tasks = from t in taskSchemas 
                                   where t.ProjectId == lastProjectSchema.Id 
                                   select new TaskData
                                   {
                                       Id = t.Id,
                                       Project = projectData,
                                       Performer = userSchemas.FirstOrDefault(u => u.Id == t.PerformerId).ToData(),
                                       Name = t.Name,
                                       Description = t.Description,
                                       State = t.State,
                                       CreatedAt = t.CreatedAt,
                                       FinishedAt = t.FinishedAt
                                   };
            projectData.Author = performerData;
            projectData.Team = teamSchemas.FirstOrDefault(t => t.Id == lastProjectSchema.TeamId).ToData();
            var longestTaskData = longestTask.ToData();
            longestTaskData.Project = projectData;
            longestTaskData.Performer = performerData;

            return (performerData, projectData, lastProjectTaskCount, countNotFinishedTasks, longestTaskData);
        }

        //  - 7 -
        public async Task<(ProjectData, TaskData, TaskData, int)> GetProjectWithMinMaxTask(int projectId)
        {
            var userSchemas = await Requests.GetUsers();
            var taskSchemas = await Requests.GetTasks();
            var teamSchemas = await Requests.GetTeams();
            var projectSchema = await Requests.GetProject(projectId);

            var projectTaskSchemas = from t in taskSchemas where t.ProjectId == projectId select t;

            var teamSchema = teamSchemas.FirstOrDefault(t => t.Id == projectSchema.TeamId);
            var countOfTeamUsers = (from u in userSchemas where u.TeamId == teamSchema.Id select u).Count();

            var projectData = projectSchema.ToData();
            var projectDataTasks = from t in projectTaskSchemas
                                select new TaskData
                                {
                                    Id = t.Id,
                                    Project = projectData,
                                    Performer = userSchemas.FirstOrDefault(u => u.Id == t.PerformerId).ToData(),
                                    Name = t.Name,
                                    Description = t.Description,
                                    State = t.State,
                                    CreatedAt = t.CreatedAt,
                                    FinishedAt = t.FinishedAt
                                };
            projectData.Tasks = projectDataTasks;
            projectData.Author = (from u in userSchemas where u.Id == projectSchema.AuthorId select u).FirstOrDefault().ToData();
            projectData.Team = teamSchema.ToData();
            var taskByDescription = (from t in projectDataTasks orderby t.Description.Length descending select t).FirstOrDefault();
            var taskByName = (from t in projectDataTasks orderby t.Name.Length select t).FirstOrDefault();

            return (projectData, taskByDescription, taskByName, countOfTeamUsers);
        }
    }
}
