﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSA21_Lecture1.DataLayer
{
    public class TaskData
    {
        public int Id {get; set;}
        public ProjectData Project {get; set;}
        public UserData Performer {get; set;}
        public string Name {get; set;}
        public string Description {get; set;}
        public int State {get; set;}
        public DateTime CreatedAt {get; set;}
        public DateTime? FinishedAt {get; set;}
    }
}
