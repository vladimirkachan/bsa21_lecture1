﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSA21_Lecture1.DataLayer
{
    public class TeamData
    {
        public int Id {get; set;}
        public string Name {get; set;}
        public DateTime CreatedAt {get; set;} 
    }
}
