﻿using System;
using System.Collections.Generic;

namespace BSA21_Lecture1.DataLayer
{
    public class UserData
    {
        public int Id { get; set; }
        public TeamData Team { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email {get; set;}
        public DateTime RegisteredAt {get; set;}
        public DateTime BirthDay {get; set;}    
    }
}
