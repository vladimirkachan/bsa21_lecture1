﻿using System;
using System.Collections.Generic;

namespace BSA21_Lecture1.DataLayer
{
    public class ProjectData
    {
        public int Id { get; set; }
        public IEnumerable<TaskData> Tasks { get; set; } = new List<TaskData>();
        public UserData Author {get; set;}
        public TeamData Team {get; set;}
        public string Name {get; set;}
        public string Description {get; set;}
        public DateTime Deadline {get; set;}
    }
}
