﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BSA21_Lecture1.WebLayer;
using BSA21_Lecture1.WebLayer.Schemas;

namespace BSA21_Lecture1.DataLayer
{
    public static class Converter
    {
        public static TeamData ToData(this TeamSchema teamSchema)
        {
            return new()
            {
                Id = teamSchema.Id,
                Name = teamSchema.Name,
                CreatedAt = teamSchema.CreatedAt
            };
        }
        public static UserData ToData(this UserSchema userSchema)
        {
            return new()
            {
                Id = userSchema.Id,
                FirstName = userSchema.FirstName,
                LastName = userSchema.LastName,
                Email = userSchema.Email,
                RegisteredAt = userSchema.RegisteredAt,
                BirthDay = userSchema.BirthDay
            };
        }
        public static TaskData ToData(this TaskSchema taskSchema)
        {
            return new()
            {
                Id = taskSchema.Id,
                Name = taskSchema.Name,
                Description = taskSchema.Description,
                State = taskSchema.State,
                CreatedAt = taskSchema.CreatedAt,
                FinishedAt = taskSchema.FinishedAt
            };
        }
        public static ProjectData ToData(this ProjectSchema projectSchema)
        {
            return new()
            {
                Id = projectSchema.Id,
                Name = projectSchema.Name,
                Description = projectSchema.Description,
                Deadline = projectSchema.Deadline
            };
        }
    }
}
