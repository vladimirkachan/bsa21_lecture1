﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using BSA21_Lecture1.WebLayer.Schemas;
using Newtonsoft.Json;

namespace BSA21_Lecture1.WebLayer
{
    public static class Requests
    {
        static readonly HttpClient client = new();
        const string host = @"https://bsa21.azurewebsites.net/api/";

        static async Task<string> GetResponseBody(HttpResponseMessage response)
        {
            return await response.Content.ReadAsStringAsync();
        }
        static async Task<string> GetEndPoint(string url)
        {
            var response = await client.GetAsync(url);
            return await GetResponseBody(response);
        }

        public static async Task<IEnumerable<ProjectSchema>> GetProjects()
        {
            var response = await client.GetAsync(host + "projects");
            return JsonConvert.DeserializeObject<IEnumerable<ProjectSchema>>(await GetResponseBody(response));
        }
        public static async Task<IEnumerable<TaskSchema>> GetTasks()
        {
            var response = await client.GetAsync(host + "tasks");
            return JsonConvert.DeserializeObject<IEnumerable<TaskSchema>>(await GetResponseBody(response));
        }
        public static async Task<IEnumerable<TeamSchema>> GetTeams()
        {
            var response = await client.GetAsync(host + "teams");
            return JsonConvert.DeserializeObject<IEnumerable<TeamSchema>>(await GetResponseBody(response));
        }
        public static async Task<IEnumerable<UserSchema>> GetUsers()
        {
            var response = await client.GetAsync(host + "users");
            return JsonConvert.DeserializeObject<IEnumerable<UserSchema>>(await GetResponseBody(response));
        }

        public static async Task<ProjectSchema> GetProject(int id)
        {
            var response = await client.GetAsync(host + $@"projects/{id}");
            return JsonConvert.DeserializeObject<ProjectSchema>(await GetResponseBody(response));
        }
        public static async Task<TaskSchema> GetTask(int id)
        {
            var response = await client.GetAsync(host + $@"tasks/{id}");
            return JsonConvert.DeserializeObject<TaskSchema>(await GetResponseBody(response));
        }
        public static async Task<TeamSchema> GetTeam(int id)
        {
            var response = await client.GetAsync(host + $@"teams/{id}");
            return JsonConvert.DeserializeObject<TeamSchema>(await GetResponseBody(response));
        }
        public static async Task<UserSchema> GetUser(int id)
        {
            var response = await client.GetAsync(host + $@"users/{id}");
            return JsonConvert.DeserializeObject<UserSchema>(await GetResponseBody(response));
        }
    }
}
