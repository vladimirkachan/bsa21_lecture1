﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace BSA21_Lecture1.WebLayer.Schemas
{
    public struct TaskSchema
    {
        [JsonProperty("id")] public int Id {get; set;}
        [JsonProperty("projectId")] public int ProjectId {get; set;}
        [JsonProperty("performerId")] public int PerformerId {get; set;}
        [JsonProperty("name")] public string Name {get; set;}
        [JsonProperty("description")] public string Description {get; set;}
        [JsonProperty("state")] public int State {get; set;}
        [JsonProperty("createdAt")] public DateTime CreatedAt {get; set;}
        [JsonProperty("finishedAt")] public DateTime? FinishedAt {get; set;}
    }
}
