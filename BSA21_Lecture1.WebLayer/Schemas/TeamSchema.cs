﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace BSA21_Lecture1.WebLayer.Schemas
{
    public struct TeamSchema
    {
        [JsonProperty("id")] public int Id {get; set;}
        [JsonProperty("name")] public string Name {get; set;}
        [JsonProperty("createdAt")] public DateTime CreatedAt {get; set;}
    }
}
