﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace BSA21_Lecture1.WebLayer.Schemas
{
    public struct ProjectSchema
    {
        [JsonProperty("id")] public int Id {get; set;}
        [JsonProperty("authorId")] public int AuthorId {get; set;}
        [JsonProperty("teamId")] public int TeamId {get; set;}
        [JsonProperty("name")] public string Name {get; set;}
        [JsonProperty("description")] public string Description {get; set;}
        [JsonProperty("deadline")] public DateTime Deadline {get; set;}

    }
}
